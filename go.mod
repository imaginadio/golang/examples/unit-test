module gitlab.com/imaginadio/golang/examples/unit-test

go 1.15

require (
	github.com/go-playground/assert/v2 v2.0.1
	gitlab.com/imaginadio/golang/sort/merge v0.0.0-20210608211816-aaf2f8c321f7
)
