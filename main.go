package main

func BubbleSort(arr []int) []int {
	result := make([]int, len(arr))
	copy(result, arr)
	for changed := true; changed; {
		changed = false
		for i := 0; i < len(result)-1; i++ {
			if result[i] > result[i+1] {
				changed = true
				buf := result[i+1]
				result[i+1] = result[i]
				result[i] = buf
			}
		}
	}
	return result
}

func SlowBubbleSort(arr []int) []int {
	result := make([]int, len(arr))
	copy(result, arr)
	for changed := true; changed; {
		changed = false
		for i := 0; i < len(result)-1; i++ {
			if result[i] > result[i+1] {
				changed = true
				buf := result[i+1]
				result[i+1] = result[i]
				result[i] = buf
				break
			}
		}
	}
	return result
}
