package main

import (
	"testing"

	"github.com/go-playground/assert/v2"
	sm "gitlab.com/imaginadio/golang/sort/merge"
)

type testData struct {
	numbers  []int
	expected []int
}

var testTable []testData = []testData{
	{
		numbers:  []int{4, 3, 2, 1},
		expected: []int{1, 2, 3, 4},
	},
	{
		numbers:  []int{1, 2, 3, 4, 0, 1, 29, 3, 1, 7, 4, 2, 67, 3, 2, 8, 7, 2},
		expected: []int{0, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 7, 7, 8, 29, 67},
	},
	{
		numbers:  []int{4, 5, 6, 1, 2, 7, 2, 6, 7, 4, 2, 6, 4, 1, 2, 4, 6, 78, 8, 9, 6, 1, 32, 5},
		expected: []int{1, 1, 1, 2, 2, 2, 2, 4, 4, 4, 4, 5, 5, 6, 6, 6, 6, 6, 7, 7, 8, 9, 32, 78},
	},
	{
		numbers:  []int{4, 9, 8, 7, 2, 1, 74, 5, 6, 4, 8, 8, 9, 9, 6, 4, 2, 8, 6, 63, 1, 2, 3, 5, 5, 4},
		expected: []int{1, 1, 2, 2, 2, 3, 4, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 8, 8, 8, 8, 9, 9, 9, 63, 74},
	},
	{
		numbers:  []int{0},
		expected: []int{0},
	},
	{
		numbers:  []int{},
		expected: []int{},
	},
}

func TestBubbleSort(t *testing.T) {
	for _, testCase := range testTable {
		result := BubbleSort(testCase.numbers)
		t.Logf("Calling BubbleSort(%v), result %v\n", testCase.numbers, result)
		assert.Equal(t, testCase.expected, result)
	}
}

func BenchmarkBubbleSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, tc := range testTable {
			arr := make([]int, len(tc.numbers))
			copy(arr, tc.numbers)
			BubbleSort(arr)
		}
	}
}

func BenchmarkSortMerge(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, tc := range testTable {
			arr := make([]int, len(tc.numbers))
			copy(arr, tc.numbers)
			sm.SortMerge(arr)
		}
	}
}

func BenchmarkFastSortMerge(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, tc := range testTable {
			arr := make([]int, len(tc.numbers))
			copy(arr, tc.numbers)
			sm.FastSortMerge(arr)
		}
	}
}
